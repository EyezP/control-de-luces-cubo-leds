
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"


/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
		
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
	
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/



static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
	


  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9 
                          |GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE6 PE7 PE8 PE9 
                           PE10 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9 
                          |GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PC13 PC14 PC15 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PD11 PD12 PD13 PD14 */
  GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PA10 PA13 PA14 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB4 PB5 PB6 PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}



int Columnas[] ={(GPIOE,GPIO_PIN_7),(GPIOE,GPIO_PIN_8),(GPIOE,GPIO_PIN_9),(GPIOE,GPIO_PIN_10),
(GPIOD,GPIO_PIN_11),(GPIOD,GPIO_PIN_12),(GPIOD,GPIO_PIN_13),(GPIOD,GPIO_PIN_14),
(GPIOA,GPIO_PIN_10),(GPIOA,GPIO_PIN_15),(GPIOA,GPIO_PIN_13),(GPIOA,GPIO_PIN_14),
(GPIOB,GPIO_PIN_4),(GPIOB,GPIO_PIN_5),(GPIOB,GPIO_PIN_6),(GPIOB,GPIO_PIN_7)};

int Niveles[]={(GPIOE,GPIO_PIN_6),(GPIOC,GPIO_PIN_13),(GPIOC,GPIO_PIN_14),(GPIOC,GPIO_PIN_15)};


int cont=0;
/*void setup()
{
	for(int i=0;i<4;i++){
		HAL_GPIO_WritePin(GPIOD,Niveles[i], 0);
}
	for(int i=0;i<16;i++){
	HAL_GPIO_WritePin(GPIOD,Columnas[i], 0);
}*/

void loop()
{
	for(int i=0;i<4;i++){
		HAL_GPIO_WritePin(GPIOD,Niveles[i], 0);
}
	for(int i=0;i<16;i++){
	HAL_GPIO_WritePin(GPIOD,Columnas[i], 0);
}
/*switch(cont){
	case 0:
HAL_GPIO_WritePin(GPIOD,Niveles[0], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[0], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[1], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[1], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[2], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[8], 1);

cont+=3;
	break;
	
	case 1:
		HAL_GPIO_WritePin(GPIOD,Niveles[2], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[15], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[3], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[12], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[1], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[9], 1);
	break;
	case 2:
		HAL_GPIO_WritePin(GPIOD,Niveles[2], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[15], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[3], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[12], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[1], 1);
HAL_GPIO_WritePin(GPIOD,Columnas[9], 1);
		
}
if(cont>=2)
	cont=0;



 HAL_Delay(100);
}*/
  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4, 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[0], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[1], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[2], 1);
	HAL_GPIO_WritePin(GPIOD,Niveles[3], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[0], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[1], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[2], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[3], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[4], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[5], 1);
  HAL_GPIO_WritePin(GPIOD,Columnas[12], 1);	
	HAL_GPIO_WritePin(GPIOD,Columnas[6], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[7], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[8], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[9], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[10], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[11], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[12], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[13], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[14], 1);
	HAL_GPIO_WritePin(GPIOD,Columnas[15], 1);
}





 
	
